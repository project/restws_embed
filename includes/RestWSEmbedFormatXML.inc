<?php

/**
 * @file
 * RESTful web services module integration for views.
 */
class RestWSEmbedFormatXML extends RestWSFormatXML {

  protected $embed_info;
  protected $current_depth = 0;
  protected $displayed;

  public function __construct($name, $info, $embed_info) {
    parent::__construct($name, $info);
    $this->embed_info = $embed_info;
  }

  public function viewResource($resourceController, $display_id) {
    $values = $this->getData($resourceController->wrapper($display_id));

    $doc = new DOMDocument('1.0', 'utf-8');
    $root = $doc->createElement($resourceController->resource());
    $doc->appendChild($root);

    $this->xmlRecursive($doc, $root, $values);

    return $doc->saveXML();
  }

  public function getData($wrapper, $isList = FALSE) {
    $data = array();
    $filtered = restws_property_access_filter($wrapper);

    $depth_increased = FALSE;
    foreach ($filtered as $name => $property) {
      try {
        if ($property instanceof EntityDrupalWrapper) {
          // For referenced entities only return the URI.
          if ($id = $property->getIdentifier()) {
            if (!$depth_increased && !$isList) {
              $this->current_depth++;
              $depth_increased = TRUE;
            }
            $data[$name] = $this->getResourceReference($property->type(), $id);
          }
        }
        elseif ($property instanceof EntityValueWrapper) {
          $data[$name] = $property->value();
        }
        elseif ($property instanceof EntityListWrapper || $property instanceof EntityStructureWrapper) {
          $data[$name] = $this->getData($property, TRUE);
        }
      } catch (EntityMetadataWrapperException $e) {
        // A property causes problems - ignore that.
      }
    }

    if ($this->current_depth > 0) {
      $this->current_depth--;
    }

    return $data;
  }

  public function getResourceReference($resource, $id) {
    if (!isset($this->displayed[$resource][$id]) && isset($this->embed_info[$resource]) && ($this->embed_info[$resource] > $this->current_depth)) {
      $this->displayed[$resource][$id] = TRUE;
            
      $info = entity_get_info($resource);
      if (isset($info['entity keys']['name'])) {
        $entities = entity_load($resource, array($id));
        $id = $entities[$id]->{$info['entity keys']['name']};
      }
      
      $wrapper = entity_metadata_wrapper($resource, $id);
      $return = $this->getData($wrapper);
    }
    else {
      $return = parent::getResourceReference($resource, $id);
    }

    return $return;
  }

  private function xmlRecursive(&$doc, &$parent, $data) {
    if (is_object($data)) {
      $data = get_object_vars($data);
    }

    if (is_array($data)) {
      $assoc = FALSE || empty($data);
      foreach ($data as $key => $value) {
        if (is_numeric($key)) {
          $key = 'item';
        }
        else {
          $assoc = TRUE;
          $key = preg_replace('/[^A-Za-z0-9_]/', '_', $key);
          $key = preg_replace('/^([0-9]+)/', '_$1', $key);
        }
        $element = $doc->createElement($key);
        $parent->appendChild($element);
        $this->xmlRecursive($doc, $element, $value);
      }

      if (!$assoc) {
        $parent->setAttribute('is_array', 'true');
      }
    }
    elseif ($data !== NULL) {
      $parent->appendChild($doc->createTextNode($data));
    }
  }

}
