<?php

/**
 * @file
 * RESTful web services module integration for views.
 */
class RestWSEmbedFormatJSON extends RestWSFormatJSON {

  protected $embed_info;
  protected $current_depth = 0;
  protected $displayed = array();

  public function __construct($name, $info, $embed_info) {
    parent::__construct($name, $info);
    $this->embed_info = $embed_info;
  }

  public function getData($wrapper, $isList = FALSE) {
    $data = array();
    $filtered = restws_property_access_filter($wrapper);

    $depth_increased = FALSE;
    foreach ($filtered as $name => $property) {
      try {
        if ($property instanceof EntityDrupalWrapper) {
          // For referenced entities only return the URI.
          if ($id = $property->getIdentifier()) {
            if (!$depth_increased && !$isList) {
              $this->current_depth++;
              $depth_increased = TRUE;
            }
            $data[$name] = $this->getResourceReference($property->type(), $id);
          }
        }
        elseif ($property instanceof EntityValueWrapper) {
          $data[$name] = $property->value();
        }
        elseif ($property instanceof EntityListWrapper || $property instanceof EntityStructureWrapper) {
          $data[$name] = $this->getData($property, TRUE);
        }
      } catch (EntityMetadataWrapperException $e) {
        // A property causes problems - ignore that.
      }
    }

    if ($this->current_depth > 0) {
      $this->current_depth--;
    }

    return $data;
  }

  public function getResourceReference($resource, $id) {
    if (!isset($this->displayed[$resource][$id]) && isset($this->embed_info[$resource]) && ($this->embed_info[$resource] > $this->current_depth)) {
      $this->displayed[$resource][$id] = TRUE;
      
      $info = entity_get_info($resource);
      if (isset($info['entity keys']['name'])) {
        $entities = entity_load($resource, array($id));
        $id = $entities[$id]->{$info['entity keys']['name']};
      }
      
      $wrapper = entity_metadata_wrapper($resource, $id);
      $return = $this->getData($wrapper);
    }
    else {
      $return = parent::getResourceReference($resource, $id);
    }

    return $return;
  }

}
